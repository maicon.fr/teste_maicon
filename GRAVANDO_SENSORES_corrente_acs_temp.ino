



#include <SPI.h>
#include <SD.h>

File myFile;




//*****PARTE DO SENSOR DE SORRENTE*******/////

#define ELECTRICITY_SENSOR A2 // Analog input pin that sensor is attached to
float amplitude_current; //amplitude current
float effective_value; //effective current





#define pinoSensor A0
int sensorValue_aux = 0;
float valorSensor = 0;
float valorCorrente = 0;
float voltsporUnidade = 0.004887586;// 5%1023
// Para ACS712 de  5 Amperes use 0.185
// Para ACS712 de 10 Amperes use 0.100
//  Para ACS712 de 5 Amperes use 0.066
float sensibilidade = 0.066;

//Tensao da rede AC 110 Volts e na verdade (127 volts)
int tensao = 220;



/////////////////////////////////////////////////

//****TEMPO PARA LEITURA DO SENSOR DE TEMP****////

#include <OneWire.h>

OneWire  ds(2);  // pino 2
#include <Wire.h>

long previousMillis = 0;        // Variável de controle do tempo
long redLedInterval = 60000;     // Tempo em ms do intervalo a ser executado // TEMPO DE 1 min
////////////////////////////////////////////////


//****SENSOR SCT013****////
#include "EmonLib.h"
 
EnergyMonitor emon1;
 
 
//Tensao da rede eletrica
int rede = 220;
 
//Pino do sensor SCT
#define pino_sct A1
////////////////////////////////////////////////





void setup(void) {
  Serial.begin(9600); 

  pinMode(pinoSensor, INPUT);

  pins_init();
  
  
  
    //Pino, calibracao - Cur Const= Ratio/BurdenR. 2000/33 = 60
  emon1.current(pino_sct, 60);



  Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");



}


void loop(void) {
  int sensor_max;
  sensor_max = getMaxValue();
  //the VCC on the Grove interface of the sensor is 5v
  amplitude_current=(float)sensor_max/1024*5/200*1000000;
  effective_value=amplitude_current/1.414;
  //minimum_current=1/1024*5/200*1000000/1.414=17.3(mA)
  //Only for sinusoidal alternating current
  Serial.print("Corrente no sensor TA12-100: ");
  Serial.print(effective_value,1);
  Serial.println(" mA");


  delay(100);


  for(int i=1000; i>0; i--){
    // le o sensor na pino analogico A0 e ajusta o valor lido ja que a saída do sensor é (1023)vcc/2 para corrente =0
    sensorValue_aux = (analogRead(pinoSensor) -510); 
    // somam os quadrados das leituras.
    valorSensor += pow(sensorValue_aux,2); 
    delay(1);
  }

  // finaliza o calculo da média quadratica e ajusta o valor lido para volts
  valorSensor = (sqrt(valorSensor/ 1000)) * voltsporUnidade; 
  // calcula a corrente considerando a sensibilidade do sernsor (185 mV por amper)
  valorCorrente = (valorSensor/sensibilidade); 

  //tratamento para possivel ruido
  //O ACS712 para 30 Amperes é projetado para fazer leitura
  // de valores alto acima de 0.25 Amperes até 30.
  // por isso é normal ocorrer ruidos de até 0.20A
  //por isso deve ser tratado
  if(valorCorrente <= 0.095){
    valorCorrente = 0; 
  }

  valorSensor =0;

  //Mostra o valor da corrente
  Serial.print("Corrente sensor efeito hall : ");
  // Irms
  Serial.print(valorCorrente, 3);
  Serial.println(" A ");

  //Calcula e mostra o valor da potencia
  //Serial.print(" Potencia (Consumo) : ");
  //Serial.print(valorCorrente * tensao);
  //Serial.println(" Watts ");

  delay(100);



    //Calcula a corrente
  double Irms = emon1.calcIrms(1480);
  //Mostra o valor da corrente no serial monitor e display
  Serial.print("Corrente sensor ACS013 : ");
  Serial.print(Irms,3); // Irms
  Serial.println(" A");
 
  
    delay(100);



  byte data[9];
  int temp, temp1;

  ds.reset();//sinal de reset
  ds.write(0xCC);//skip rom
  ds.write(0x44);//inicia conversão de temperatura
  delay(750);//espera 750ms para calcular a temperatura
  ds.reset();//sinal de reset
  ds.write(0xCC);//skip rom
  ds.write(0xBE);//inicia a leitura do scratchpad ( 9 bytes )

  //Leitura
  for (byte i = 0; i < 9; i++) {
    data[i] = ds.read();
  }


  if ((data[1] >> 7) == 1) { //verifica se a temperatura é negativa
    data[1] = ~data[1];
    data[0] = (~data[0]) + 1;

    Serial.print("-");
  }
  else {

    Serial.print("+");

  }
  temp1 = (data[1] << 4) | (data[0] >> 4);

  Serial.print(temp1);


  Serial.print(".");

  temp = (data[0] & 0x0F) * 625;
  if (temp > 625) {

    Serial.print(temp);
  }
  else {

    Serial.print("0");

    Serial.print(temp);
  }

  Serial.print(" ");



  Serial.println("C    ");







  delay(100);





  //while (Irms < 1);

  // GRAVANDO A CORRENTE DO TA12-100 //

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test1.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test1.txt...");
    //myFile.print(temp1);
    //myFile.print(".");
    //myFile.println(temp);
    myFile.println(effective_value);
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test1.txt");
  }

  // re-open the file for reading:
  myFile = SD.open("test1.txt");
  if (myFile) {
    Serial.println("test1.txt:");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test1.txt");
  }
  
  
    // GRAVANDO A CORRENTE DO ACS712 //

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test2.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test2.txt...");
    //myFile.print(temp1);
    //myFile.print(".");
    //myFile.println(temp);
    myFile.println(valorCorrente);
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test2.txt");
  }

  // re-open the file for reading:
  myFile = SD.open("test2.txt");
  if (myFile) {
    Serial.println("test2.txt:");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test2.txt");
  }


    // GRAVANDO A CORRENTE DO SCT013 //

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test3.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test3.txt...");
    //myFile.print(temp1);
    //myFile.print(".");
    //myFile.println(temp);
    myFile.println(valorCorrente);
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test3.txt");
  }

  // re-open the file for reading:
  myFile = SD.open("test3.txt");
  if (myFile) {
    Serial.println("test3.txt:");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test3.txt");
  }


  // GRAVANDO A TEMPERATURA //

  unsigned long currentMillis = millis();    //Tempo atual em ms
  //Lógica de verificação do tempo
  if (currentMillis - previousMillis > redLedInterval) {
    previousMillis = currentMillis;    // Salva o tempo atual

    // open the file. note that only one file can be open at a time,
    // so you have to close this one before opening another.
    myFile = SD.open("test4.txt", FILE_WRITE);

    // if the file opened okay, write to it:
    if (myFile) {
      Serial.print("Writing to test4.txt...");
      myFile.print(temp1);
      myFile.print(".");
      myFile.println(temp);
      // close the file:
      myFile.close();
      Serial.println("done.");
    } else {
      // if the file didn't open, print an error:
      Serial.println("error opening test4.txt");
    }

    // re-open the file for reading:
    myFile = SD.open("test4.txt");
    if (myFile) {
      Serial.println("test4.txt:");

      // read from the file until there's nothing else in it:
      while (myFile.available()) {
        Serial.write(myFile.read());
      }
      // close the file:
      myFile.close();
    } else {
      // if the file didn't open, print an error:
      Serial.println("error opening test4.txt");
    }
  }
}



void pins_init()
{
pinMode(ELECTRICITY_SENSOR, INPUT);
pinMode(pino_sct, INPUT);
pinMode(pinoSensor, INPUT);

}
/*Function: Sample for 1000ms and get the maximum value from the SIG pin*/
int getMaxValue()
{
int sensorValue; //value read from the sensor
int sensorMax = 0;
uint32_t start_time = millis();
while((millis()-start_time) < 1000)//sample for 1000ms
{
sensorValue = analogRead(ELECTRICITY_SENSOR);
if (sensorValue > sensorMax)
{
/*record the maximum sensor value*/
sensorMax = sensorValue;
}
}
return sensorMax;
}



